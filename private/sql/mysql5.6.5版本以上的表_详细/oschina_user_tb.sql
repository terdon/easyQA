CREATE TABLE `oschina_user_tb`( 
   `openid` INT UNSIGNED NOT NULL COMMENT '开源中国用户ID', 
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(36) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用开源中国登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用开源中国登录时间', 
   PRIMARY KEY `openid_pk` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='开源中国登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;