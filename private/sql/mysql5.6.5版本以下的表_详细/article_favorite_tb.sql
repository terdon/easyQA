CREATE TABLE `article_favorite_tb`(
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID',
   `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
   `add_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '收藏时间',
   PRIMARY KEY id_pk (`id`),
   INDEX userId_index (`user_id`),
   INDEX articleId_index (`article_id`)
 )  ENGINE=MYISAM COMMENT='文章收藏表' ROW_FORMAT=FIXED CHARSET=utf8;