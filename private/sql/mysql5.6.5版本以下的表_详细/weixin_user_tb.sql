CREATE TABLE `weixin_user_tb`( 
   `unionid` CHAR(32) NOT NULL COMMENT '微信unionid',
   `openid` CHAR(32) NOT NULL COMMENT '微信openid',
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(128) NOT NULL , 
   `signup_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用微信登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用微信登录时间', 
   PRIMARY KEY `unionid_pk` (`unionid`),
   UNIQUE INDEX `openid_uq` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='微信登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;